export interface IRole {
  id: number;
  position: string;
}