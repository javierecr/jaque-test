import { User } from "../models/user";
import { IUser } from "../interfaces/user.interface";

export class UsersResponse {
  users: Array<IUser> = [];

  constructor(response: any) {
    response.users.forEach((pUser: any) => {
      this.users.push(new User(pUser));
    });
  }
}