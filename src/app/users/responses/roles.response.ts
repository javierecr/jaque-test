import { Role } from './../models/role';
import { IRole } from './../interfaces/role.interface';

export class RolesResponse {
  roles: Array<IRole> = [];

  constructor(response: any) {
    response.roles.forEach((pRole: any) => {
      this.roles.push(new Role(pRole));
    });
  }
}