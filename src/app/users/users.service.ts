import { RolesResponse } from './responses/roles.response';
import { Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from 'rxjs/operators';
import { User } from "./models/user";
import { UsersResponse } from "./responses/users.response";

@Inject({})

export class UsersService {
  constructor(private http: HttpClient) {

  }

  getUsers(): Observable<UsersResponse> {
    return this.http
      .get(`assets/data/users.json`)
      .pipe(map(response => response as UsersResponse))
  }

  getRoles(): Observable<RolesResponse> {
    return this.http
      .get(`assets/data/roles.json`)
      .pipe(map(response => response as RolesResponse))
  }
}