import { IUser } from "../interfaces/user.interface";

export class User {
  picture: string;
  name: string;
  fathersLastName: string;
  mothersLastName: string;
  email: string;
  roleId: number;
  active: boolean;
  role: string = ''

  constructor (attrs: IUser) {
    this.picture = attrs.picture
    this.name = attrs.name
    this.fathersLastName = attrs.fathersLastName
    this.mothersLastName = attrs.mothersLastName
    this.email = attrs.email
    this.roleId = attrs.roleId
    this.active = attrs.active
  }

}