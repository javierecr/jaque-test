import { IRole } from './../interfaces/role.interface';

export class Role {
  id: number;
  position: string;

  constructor (attrs: IRole) {
    this.id = attrs.id
    this.position = attrs.position
  }
}