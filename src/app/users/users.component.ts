import { ModalService } from '../_modal/modal.service';
import { RolesResponse } from './responses/roles.response';
import { Role } from './models/role';
import { UsersResponse } from './responses/users.response';
import { UsersService } from './users.service';
import { User } from './models/user';
import { map } from 'rxjs/operators';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  @ViewChild('file') file: ElementRef

  userForm: FormGroup

  users: Array<User>
  roles: Array<Role>
  isEdittingUser: boolean
  edittingUserIndex: number

  constructor(
    private usersService: UsersService,
    private modalService: ModalService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.setUpUserForm()
    this.getUsers()
  }

  closeModal(id: string) {
    this.setUpUserForm
    this.modalService.close(id);
  }

  loadNewProfilePicture(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];

      this.showFilePreview(file);
    }
  }

  onActiveToggle(index) {
    let user = this.users[index]

    if (user.active) {
      user.active = false
    } else {
      user.active = true
    }
  }

  onSubmit(): void {
    let formRoleId = this.userForm.get('roleId').value

    this.userForm.patchValue({
      roleId: parseInt(formRoleId)
    })

    const user = new User(this.userForm.value)
    const role = this.roles.find(pRole => {
      return pRole.id === user.roleId
    })

    user.role = role.position

    if (this.isEdittingUser) {
      this.users[this.edittingUserIndex] = user

      this.isEdittingUser = false
      this.setUpUserForm()
    } else {
      this.users.unshift(user)
    }
    this.modalService.closeAll()
  }

  onUserDelete(index: number): void {
    this.users.splice(index, 1)
  }  

  onUserEdit(user, index) {
    this.edittingUserIndex = index
    this.isEdittingUser = true;
    this.userForm.patchValue(user)

    this.modalService.open('modal')
  }

  openModal(id: string) {
    this.modalService.open(id);
  }

  

  private showFilePreview(file) {
    var reader = new FileReader();

    reader.readAsDataURL(file);
    reader.onload = (_event) => {
      this.userForm.patchValue({ picture: reader.result});
    }
  }

  private assignUserRoles() {
    this.users.forEach((pUser: User) => {
      const roleName = this.roles.find(pRole => {
        return pRole.id === pUser.roleId
      }).position

      pUser.role = roleName
    })
  }

  private getUsers() {
    this.usersService
      .getUsers()
      .pipe(
        map((response: UsersResponse) => {
          return new UsersResponse(response)
        })
      )
      .subscribe((pUsers: UsersResponse) => {
        this.users = pUsers.users
        this.getRoles()
      }, error => {
        console.error(error)
      })
  }

  private setUpUserForm() {
    this.userForm = this.fb.group({
      name: new FormControl('', Validators.required),
      fathersLastName: new FormControl('', Validators.required),
      mothersLastName: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      roleId: new FormControl('', Validators.required),
      active: new FormControl(false),
      picture: new FormControl(''),
      isExistingUser: new FormControl(false)
    })
  }

  private getRoles() {
    this.usersService
      .getRoles()
      .pipe(
        map((response: RolesResponse) => {
          return new RolesResponse(response)
        })
      )
      .subscribe((pRoles: RolesResponse) => {
        this.roles = pRoles.roles
        this.assignUserRoles()
      }, error => {
        console.error(error)
      })
  }
}
